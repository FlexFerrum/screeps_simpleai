var actions = require('gameaction');

var roomIsMine = function(roomName)
{
    var room = Game.rooms[roomName];
    if (!room)
        return false;
    
    return room.controller.my;
};

var roomActivities = [
    {
        activityId : actions.ROOM_ACTIVITY_USERESOURCES,
        activityField : 'useResourcesActivity',
        precondition : (roomName) => (_.filter(Memory.roomsInUse, (rn) => rn === roomName)).length != 0 && !roomIsMine(roomName),
        processor : undefined
    },
    {
        activityId : actions.ROOM_ACTIVITY_MANAGEENERGY,
        activityField : 'manageEnergyActivity',
        precondition : roomIsMine,
        processor : undefined
    },
    {
        activityId : actions.ROOM_ACTIVITY_DEVELOPROOM,
        activityField : 'developRoomActivity',
        precondition : roomIsMine,
        processor : undefined
    },
    {
        activityId : actions.ROOM_ACTIVITY_DEFENDROOM,
        activityField : 'defendRoomActivity',
        precondition : (roomName) => (_.filter(Memory.roomsInUse, (rn) => rn === roomName)).length != 0,
        processor : undefined
    },
    {
        activityId : actions.ROOM_ACTIVITY_SECUREROOM,
        activityField : 'secureRoomActivity',
        precondition : roomIsMine,
        processor : undefined
    },
];

var roomManagerImpl = {

    manageRoomLevel2 : function(spawn) {
        var room = spawn.room;
        var extensions = room.find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_EXTENSION }
        });
        console.log('Spawn has '+extensions.length+' extensions available');
        
        if (extensions.length == 5)
            return;

        if (room.find(FIND_CONSTRUCTION_SITES) != 0)
            return;
        
        var pos = spawn.pos;
        var xdeltas = [-2, -1, 0, 1, 2];
        var xdelta = xdeltas[extensions.length];
        console.log('==== xdelta: ' + xdelta);
        var result = spawn.room.createConstructionSite(pos.x + xdelta, pos.y + 2, STRUCTURE_EXTENSION);
        console.log("Extension creation result: " + result);
    },
    
    buildRoads : function(spawn)
    {
        var room = spawn.room;
        var constructionSites = room.find(FIND_CONSTRUCTION_SITES);
        if (constructionSites.length >= 2)
            return;
        
        var usedPos1;
        var usedPos2;
        
        for (var roomPosName in room.memory.usedPositions)
        {
            var roomPosInfo = room.memory.usedPositions[roomPosName];
            
            var look = room.lookAt(roomPosInfo.x, roomPosInfo.y);
            
            var hasRoad = false;
            look.forEach(function (lookObject)
            {
                if (lookObject.type == LOOK_STRUCTURES && lookObject[LOOK_STRUCTURES].structureType == STRUCTURE_ROAD)
                {
                    hasRoad = true;
                    return;
                }
                if (lookObject.type == LOOK_CONSTRUCTION_SITES)
                {
                    hasRoad = true;
                    return;
                }
            });
            
            if (hasRoad)
                continue;
            
            if (usedPos1 == undefined)
            {
                usedPos1 = roomPosInfo;
                continue;
            }
            
            if (usedPos1.used < roomPosInfo.used)
            {
                usedPos2 = usedPos1;
                usedPos1 = roomPosInfo;
            }
        }
        
        if (usedPos1 != undefined && usedPos1.used >= 100)
        {
            var result = spawn.room.createConstructionSite(usedPos1.x, usedPos1.y, STRUCTURE_ROAD);
            console.log('Road creation result: (' + usedPos1.x + ', ' + usedPos1.y + '): ' + result);            
        }
        
        if (usedPos2 != undefined && usedPos2.used >= 10 && constructionSites.length == 1)
        {
            var result = spawn.room.createConstructionSite(usedPos2.x, usedPos2.y, STRUCTURE_ROAD);
            console.log('Road creation result at (' + usedPos2.x + ', ' + usedPos2.y + '): ' + result);            
        }
    }
};

var roomManager = {
    setupOwnedRoomActivity : function(room)
    {
        room.memory.developmentActivity = setupRoomActivity(room);
        room.memory.secureActivity = setupRoomActivity(room);
    },
    
    addRoomAction : function(roomName, actionId, extraParams)
    {
        var roomMemory = Memory.rooms[roomName];
        if (!roomMemory)
        {
            console.log('ERROR: Can\'t add action to the room "' + roomName + '" queue. This room is unknown!!!!');
            return false;
        }
        
        if (!roomMemory.actionsQueue)
            roomMemory.actionsQueue = {queueId : 'room-' + roomName};

        var actionParams = {actionId: actionId, extraParams: extraParams};

        actions.pushAction(roomMemory, actionParams);
    },
    
    run : function(spawn) {
        var curLevel = spawn.room.controller.level;
        
//        if (curLevel == 2) 
//            roomManagerImpl.manageRoomLevel2(spawn);
        
        // roomManagerImpl.buildRoads(spawn);
        spawn.room.defend();
    },
    
    processAction : function(roomName, room, roomMemory, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            default:
                console.log('>>>>>> ERROR!!!! Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },
    
    processRoomActivity : function(roomName)
    {
        var room = Game.rooms[roomName];
        var memory = Memory.rooms[roomName];

        for (var idx in roomActivities)
        {
            var activityInfo = roomActivities[idx];
            
            if (activityInfo.precondition(roomName))
            {
                if (activityInfo.processor)
                {
                    console.log('Trying run activity ' + activityInfo.activityField + ' for room "' + roomName + '"');
                    if (!memory[activityInfo.activityField])
                        memory[activityInfo.activityField] = {};
                    
                    activityInfo.processor(roomName, room, memory[activityInfo.activityField], memory);
                }
                else
                {
                    console.log('ERROR!!! Processor not defined for activity ' + activityInfo.activityField + ' for room "' + roomName + '"');                
                }
            }
            else
            {
                console.log('Precondition failed for activity ' + activityInfo.activityField + ' for room "' + roomName + '"');                
            }
        }
        
        actions.processQueueAndActions(memory, (ai) => this.processAction(roomName, room, memory, actionInfo), 5);
        
        if (room)
        {
            room.run();
        }
    }
};

module.exports = roomManager;