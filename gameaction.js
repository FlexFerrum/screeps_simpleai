module.exports = {
    SPAWN_REMOTEHARVESTER : 0x0100,
    SPAWN_REMOTEBUILDER : 0x0102,
    SPAWN_REMOTEDEFENDER : 0x0103,
    SPAWN_LORRY : 0x0104,
    SPAWN_SCOUT : 0x0105,
        
    GAME_DISCOVERROOM : 0x0000,
    GAME_USEROOM : 0x0001,
    GAME_CLAIMROOM : 0x0002,
    GAME_CAPTUREROOM : 0x0003,
    
    CREEP_GENERAL_HARVEST : 0x10000,
    CREEP_GENERAL_BUILD : 0x10001,
    CREEP_GENERAL_COLLECTENERGY : 0x10002,
    CREEP_GENERAL_UPGRADECONTROLLER : 0x10003,
    
    CREEP_SCOUT_DISCOVER : 0x10100,
    
    CREEP_REMOTEHARVESTER_FINDROOM : 0x10200,
    CREEP_REMOTEHARVESTER_BUILDCONTAINER : 0x10201,
    CREEP_REMOTEHARVESTER_HARVEST : 0x10202,
    CREEP_REMOTEHARVESTER_RENEW : 0x10202,
    
    CREEP_REMOTEBUILDER_FINDROOM : 0x10300,
    CREEP_REMOTEBUILDER_HARVEST : 0x10301,
    CREEP_REMOTEBUILDER_BUILD : 0x10301,

    CREEP_LORRY_TRANSPORTGOODS : 0x10400,
    
    state : {
        Created : 0,
        InProgress : 1,
        Finished : 2,
        Cancelled : 3
    },
    
    pushAction : function(queue, actionInfo)
    {
        if (!Memory.actionId)
            Memory.actionId = 1;
        else
            Memory.actionId ++;

        if (!queue.queue)
            queue.queue = [];

        actionInfo.id = Memory.actionId;
        actionInfo.state = this.state.Created;
        queue.queue.push(actionInfo);

        console.log('>>>>> New ' + queue.queueId + ' action added: ' + JSON.stringify(actionInfo));        
    },

    processQueue : function(queue, activeActions, maxActiveActions)
    {
        if (!queue || !queue.queue || !activeActions || queue.queue.length === 0)
            return false;
        
        var currentActiveActions = _.filter(activeActions, (ai) => ai.state === this.state.Created || ai.state === this.state.InProgress);
        while (currentActiveActions.length < maxActiveActions)
        {
            var newAction = queue.queue.shift();
            if (newAction)
                activeActions.push(newAction);
            else
                break;
        }
    },

    removeFinishedActions : function(activeActions)
    {
        var doFinish = false;
        
        while (!doFinish)
        {
            var idx = 0;
            var doRemove = false;
            for (; idx < activeActions.length; ++ idx)
            {
                var a = activeActions[idx];
                if (a.state === this.state.Finished || a.state === this.state.Cancelled)
                {
                    doRemove = true;
                    break;
                }
            }
            
            if (doRemove)
                activeActions.splice(idx, 1);
            else
                doFinish = true;
        }
    },
    
    processQueueAndActions : function(memory, processor, maxActiveActions, queueName, activeActionsName)
    {
        if (queueName === undefined)
            queueName = 'actionsQueue';
        
        if (activeActionsName === undefined)
            activeActionsName = 'actionsInProgress';
        
        if (!memory[activeActionsName])
            memory[activeActionsName] = [];
        
        var queue = memory[queueName];
        var activeActions = memory[activeActionsName];
            
        this.processQueue(queue, activeActions, maxActiveActions);
        var actions = this;
        _.each(activeActions, function(actionInfo) {
            if (actionInfo.state === actions.state.Finished || actionInfo.state === actions.state.Cancelled)
                return;
            
            processor(actionInfo);
        });
        
        // this.removeFinishedActions(activeActions);
    },
    
    toString : function(action)
    {
        return JSON.stringify(action);
    }
}
