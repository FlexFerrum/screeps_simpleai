/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global FIND_SOURCES_ACTIVE, ERR_NOT_IN_RANGE */



var creepsUtilities = {
    CreepRoleHarvester: 1,
    CreepRoleUpgrader: 2,
    CreepRoleRepairer: 3,
    CreepRoleClaimer: 4,
    CreepRoleScout: 5,
    CreepRoleBuilder: 6,
    CreepRoleDefender: 7,
    
    roles : {
        Harvester: 1,
        Upgrader: 2,
        Repairer: 3,
        Claimer: 4,
        Scout: 5,
        Builder: 6,
        Defender: 7,
        RemoteHarvester: 8,
        RemoteBuilder: 9,
        Lorry : 10
    },
    
    getCreepHome: function (creep)
    {
        return creep.homeRoom();
    },
    
    harvestEnergy: function (creep, roomName, track)
    {
        if (roomName === undefined)
            roomName = creep.memory.homeRoom;
        
        if (track)
            console.log('>>>> Room to harvest energy: ' + roomName);
//        var resource = creep.pos.findClosestByRange(RESOURCE_ENERGY);
//        if (resource != undefined)
//        {
//            if (creep.pickup(resource) === ERR_NOT_IN_RANGE)
//                creep.moveTo(resource);
//            return;
//        }
//        var moveParts = _.sum(creep.body, (part) => part.type == MOVE);

        var source;
        
        source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE, {
                filter: (s) => s.pos.roomName === roomName
            });
            
        if (!source)
        {
            var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
                             && s.store[RESOURCE_ENERGY] > 0
                             && s.pos.roomName === roomName
                             && roomName === creep.memory.homeRoom
                    });
                    
            if (target)
            {
                if (target.transfer(creep, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE)
                    creep.moveTo(target);
                
                return;
            }                    
        }
        else
        {
            var res = creep.harvest(source);
            if (res === ERR_NOT_IN_RANGE)
                creep.moveTo(source);
        }
                
    },
    
    findEnergy: function (creep, roomName, track)
    {
        if (roomName === undefined)
            roomName = creep.memory.homeRoom;
        
        if (track)
            console.log('>>>> Room to find energy: ' + roomName);

        var source;
        
        var source = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE)
                         && s.store[RESOURCE_ENERGY] > 0
                         && s.pos.roomName === roomName
                         && roomName === creep.memory.homeRoom
                });

        if (!source)
        {
            source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE, {
                    filter: (s) => s.pos.roomName === roomName
                });
            
            if (source)
            {
                if (target.transfer(creep, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE)
                    creep.moveTo(target);
                
                return;
            }                    
        }
        else
        {
            var res = creep.harvest(source);
            if (res === ERR_NOT_IN_RANGE)
                creep.moveTo(source);
        }
                
    },

    collectEnergy: function (creep, track)
    {
        var room = this.getCreepHome(creep);
        if (track)
            console.log('Room for structures: ' + room);
        
        var startPos = creep.pos;
        if (creep.pos.roomName != room.name)
            startPos = room.spawns[0].pos;
        
        var target = startPos.findClosestByRange(FIND_MY_STRUCTURES, {
            filter: (s) => (s.structureType == STRUCTURE_SPAWN
                         || s.structureType == STRUCTURE_EXTENSION
                         || s.structureType == STRUCTURE_TOWER)
                         && s.energy < s.energyCapacity
                         && s.pos.roomName === creep.memory.homeRoom
                });
                
        if (track)
            console.log('>>>>> Target for energy store #1: ' + target);
        
        if (!room)
            return false;
        
        if (!target)
        {
            target = startPos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
                             && s.store[RESOURCE_ENERGY] < s.storeCapacity
                             && s.pos.roomName === creep.memory.homeRoom
                    });
        }
                
                
        if (!target)
            target = room.storage;

        // if we found one
        if (target)
        {
            // try to transfer energy, if it is not in range
            if (creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
            {
                // move towards it
                creep.moveTo(target);
            }
            return true;
        }
        return false;
    },

    buildSite: function (creep, target)
    {
        var room = this.getCreepHome(creep);
        if (!target)
            target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES, {
                filter: (s) => s.pos.roomName === creep.memory.homeRoom
                    });

        if (!target)
        {
            console.log('====  No site to build found');
            return false;
        }
        else
            console.log('====  Found site to build: ' + target);
                
        // try to transfer energy, if it is not in range
        if (creep.build(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
        {
            // move towards it
            creep.moveTo(target);
        }
        return true;
    },
    
    upgradeController: function(creep)
    {
        var room = this.getCreepHome(creep);
        if (!room)
            return;
        
        if (creep.upgradeController(room.controller) == ERR_NOT_IN_RANGE)
        {
            creep.moveTo(room.controller);
        }
    },

    checkWorkingMode: function (creep)
    {
        if (creep.memory.isWorking && creep.carry.energy == 0)
        {
            creep.memory.isWorking = false;
            creep.say('harvesting');
            return;
        }

        if (!creep.memory.isWorking && creep.carry.energy == creep.carryCapacity)
        {
            creep.memory.isWorking = true;
            creep.say('working');
        }
    },

    roleName: function (roleId)
    {
        var result = 'unknown role';
        switch (roleId)
        {
            case creepsUtilities.CreepRoleUpgrader:
                result = 'Updater';
                break;
            case creepsUtilities.CreepRoleHarvester:
                result = 'Harvester';
                break;
            case creepsUtilities.CreepRoleRepairer:
                result = 'Repairer';
                break;
            case creepsUtilities.CreepRoleClaimer:
                result = 'Claimer';
                break;
            case creepsUtilities.CreepRoleScout:
                result = 'Scout';
                break;
            case creepsUtilities.CreepRoleBuilder:
                result = 'Builder';
                break;
            case creepsUtilities.CreepRoleDefender:
                result = 'Defender';
                break;
            case this.roles.RemoteHarvester:
                result = 'RemoteHarvester';
                break;
            case this.roles.RemoteBuilder:
                result = 'RemoteBuilder';
                break;
            case this.roles.Lorry:
                result = 'Lorry';
                break;
        }

        return result;
    },

    createCreep: function (spawn, props, role, memory)
    {
        var result = spawn.canCreateCreep(props);

        if (result !== OK)
            return result;
        
        memory.roleId = role;
        memory.isWorking = false;
        memory.homeRoom = spawn.room.name;
        memory.justInitialized = true;

        var creepName = this.roleName(role) + '_' + Game.time + '_' + spawn.room.name;
        var spawningResult = spawn.createCreep(props, creepName, memory);
        return spawningResult;
    },
    
    initCreep : function (creep)
    {
        // creep.name = this.roleName(creep.memory.roleId) + '_' + creep.name + '_' + this.getCreepHome(creep);
        creep.memory.justInitialized = false;
    }
};

module.exports = creepsUtilities;

