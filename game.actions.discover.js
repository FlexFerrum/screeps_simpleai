var actions = require('gameaction');
var creepsUtilities = require('creeps.utilities');

var gameActionDiscrover = {
initDiscovering : function(actionInfo)
{
    actionInfo.state = actions.state.InProgress;
    
    Memory.roomsToDiscover.push(actionInfo.roomName);
    actionInfo.stage = 'findScout';
},

sendScout : function(actionInfo, scout)
{
    scout.addAction(actions.CREEP_SCOUT_DISCOVER, {targetRoom: actionInfo.roomName, sourceRoom: actionInfo.fromRoomName, sourceDirection: actionInfo.direction});
    actionInfo.stage = 'waitDiscover';
    actionInfo.creepId = scout.id;  
},

findScout : function(actionInfo, strategy)
{
    var scouts = _.filter(Game.creeps, (c) => c.roleId == creepsUtilities.roles.Scout);
    
    if (!scouts || scouts.length == 0)
    {
        actionInfo.stage = 'createScout';
        return;
    }

    this.sendScout(actionInfo, scouts[0]);
},

createScout : function(actionInfo, strategy)
{
    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
    
    firstRoom.addSpawnAction(actions.SPAWN_SCOUT, {});
    actionInfo.stage = 'waitScout';
},

waitScout : function(actionInfo, strategy)
{
    var scouts = _.filter(Game.creeps, (c) => c.roleId == creepsUtilities.roles.Scout);
    
    if (!scouts || scouts.length == 0)
        return;

    this.sendScout(actionInfo, scouts[0]);
},

waitDiscover : function(actionInfo, strategy)
{
    var scouts = _.filter(Game.creeps, (c) => c.roleId == creepsUtilities.roles.Scout);
    var scout = scouts[0];
    
    if (scout.pos.roomName !== actionInfo.roomName)
        return;
    
    actionInfo.stage = 'exploreRoom';
    actionInfo.roomEntrancePos = {x : scout.pos.x, y : scout.pos.y};
},

exploreRoom : function(actionInfo, strategy)
{
    console.log('Try neighbour room "' + actionInfo.roomName + '"');

    var sourceRoom = Game.rooms[actionInfo.fromRoomName];
    var room = Game.rooms[actionInfo.roomName];
    var roomInfo = sourceRoom.memory[actionInfo.direction];
    
    console.log('Source room: ' + sourceRoom + ', targetRoom: ' + room);

    var roomController = room.controller;
    var origType = roomInfo.type;
    if (roomController.my)
        roomInfo.type = 'controlled';

    if (roomController.owner !== undefined)
        roomInfo.type = 'hostile';

    if (_.filter(Memory.roomInUse, (r) => r == roomName).length !== 0)
        roomInfo.type = 'used';
    
    if (roomInfo.type === origType)
    {
        roomInfo.type = 'neutral';
        var newActionInfo = {
            actionId : actions.GAME_USEROOM,
            roomName : actionInfo.roomName,
            fromRoomName : actionInfo.fromRoomName,
            direction : actionInfo.direction
        };
        
        strategy.addGameAction(newActionInfo);
        
        room.createFlag(actionInfo.roomEntrancePos.x, actionInfo.roomEntrancePos.x, actionInfo.roomName + '_' + actionInfo.fromRoomName + '_Entrance');
    }
    actionInfo.state = actions.state.Finished;
},
    
doAction : function(actionInfo, strategy)
{
    if (!Memory.roomsToDiscover)
        Memory.roomsToDiscover = [];
    
    // if (!_.contains(Memory.roomsToDiscover, actionInfo.roomName))
    if (!actionInfo.stage)
        this.initDiscovering(actionInfo);
    

    console.log('GAME_DISCOVERROOM action stage: \'' + actionInfo.stage + '\'');
    this[actionInfo.stage](actionInfo, strategy);
}
};

module.exports = gameActionDiscrover;