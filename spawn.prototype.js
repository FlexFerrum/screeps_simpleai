var actions = require('gameaction');
var creepsManager = require('creeps.manager');

StructureSpawn.prototype.addAction = function (actionId, extraParams)
{
    if (!this.memory.actionsQueue)
        this.memory.actionsQueue = {queueId : 'spawn-' + this.id};

    var actionParams = {actionId: actionId, extraParams: extraParams};
    
    actions.pushAction(this.memory.actionsQueue, actionParams);
};

StructureSpawn.prototype.spawnCreep = function(actionInfo, creepCreator)
{
    // actionInfo.state = creepCreator(this) ? actions.state.Finished : actions.state.InProgress;
};

StructureSpawn.prototype.processAction = function(actionInfo)
{
    switch (actionInfo.actionId)
    {
        case actions.SPAWN_SCOUT:
            // spawnCreep(actionInfo, creepsManager.createScout);
            actionInfo.state = creepsManager.createScout(this, actionInfo.extraParams) ? actions.state.Finished : actions.state.InProgress;
            break;
        case actions.SPAWN_REMOTEHARVESTER:
            // spawnCreep(actionInfo, creepsManager.createScout);
            actionInfo.state = creepsManager.createRemoteHarvester(this, actionInfo.extraParams) ? actions.state.Finished : actions.state.InProgress;
            break;
        case actions.SPAWN_REMOTEBUILDER:
            // spawnCreep(actionInfo, creepsManager.createScout);
            actionInfo.state = creepsManager.createRemoteBuilder(this, actionInfo.extraParams) ? actions.state.Finished : actions.state.InProgress;
            break;
        case actions.SPAWN_LORRY:
            // spawnCreep(actionInfo, creepsManager.createScout);
            actionInfo.state = creepsManager.createLorry(this, actionInfo.extraParams) ? actions.state.Finished : actions.state.InProgress;
            break;
        default:
            console.log('!!!!!! Unkown action: ' + actions.toString(actionInfo));
            break;
    };
};

StructureSpawn.prototype.processQueue = function()
{
    actions.processQueueAndActions(this.memory, (ai) => this.processAction(ai), 1);
    // 
    
    /*
    for (var idx in this.memory.actionsInProgress)
    {
        var a = currentActions[idx];
        if (actions.isFinished(a))
            continue;
        
        switch (a.actionId)
        {
            case actions.SPAWN_SCOUT:
                this.spawnCreep(a, creepsManager.createScout);
                break;
            default:
                console.log('!!!!!! Unkown action: ' + actions.toString(a));
                break;
        };
    }
    
    actions.removeFinishedActions(this.memory.actionsInProgress);
    */
};
    

