var actions = require('gameaction');
var creepsUtilities = require('creeps.utilities');

var gameActionImpl = {
init : function(actionInfo)
{
    actionInfo.state = actions.state.InProgress;
    
    Memory.roomsInUse.push(actionInfo.roomName);
    actionInfo.stage = 'createHarvester';
},

sendHarvester : function(actionInfo, harvester)
{
    harvester.addAction(actions.CREEP_REMOTEHARVESTER_FINDSOURCE, {targetRoom: actionInfo.roomName, sourceRoom: actionInfo.fromRoomName, flagColor: COLOR_WHITE});
    actionInfo.stage = 'createBuilder';
    actionInfo.harvesterCreepId = harvester.id;  
},

sendBuilder : function(actionInfo, builder)
{
    builder.addAction(actions.CREEP_REMOTEHARVESTER_FINDSOURCE, {targetRoom: actionInfo.roomName, sourceRoom: actionInfo.fromRoomName, flagColor: COLOR_WHITE});
    actionInfo.stage = 'uknown';
    actionInfo.builderCreepId = builder.id;
},

createHarvester : function(actionInfo, strategy)
{
    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
    
    firstRoom.addSpawnAction(actions.SPAWN_REMOTEHARVESTER, {roomToHarvest: actionInfo.roomName, roomToStore: actionInfo.roomName});
    actionInfo.stage = 'waitHarvester';
},

createBuilder : function(actionInfo, strategy)
{
    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
    
    firstRoom.addSpawnAction(actions.SPAWN_REMOTEBUILDER, {});
    actionInfo.stage = 'buildContainer';
},

waitHarvester : function(actionInfo, strategy)
{
    var harvesters = _.filter(Game.creeps, (c) => c.roleId === creepsUtilities.roles.RemoteHarvester);
    
    console.log('>>> WaitHarvesters. Harvesters count: ' + harvesters.length);
    if (!harvesters || harvesters.length == 0)
        return;

    this.sendHarvester(actionInfo, harvesters[0]);
},

buildContainer : function(actionInfo, strategy)
{
    // return;
    // var harvester = Game.creeps[actionInfo.creepId];
    var harvester = _.filter(Game.creeps, (c) => c.id === actionInfo.harvesterCreepId)[0];
    var spawn = Game.rooms[Memory.myRooms[0].roomName].spawns[0];
    
    console.log('Found remote harvester: ' + JSON.stringify(harvester));
    var hPos = harvester.pos;
    console.log('Found remote harvester at: ' + JSON.stringify(hPos));
    
    var eSource = harvester.pos.findClosestByPath(FIND_SOURCES, {filter: (s) => s.pos.roomName === harvester.pos.roomName});
    console.log('Found energy source to use: ' + JSON.stringify(eSource));

    var path = spawn.pos.findPathTo(eSource);
    console.log('Path to energy source to use: ' + JSON.stringify(path));
    
    var len = path.length;
    var posIdx = 0;
    
    if (len < 10)
        posIdx = len / 2;
    else
        posIdx = len - 10;
    
    var room = Game.rooms[harvester.pos.roomName];
    var objects = room.lookAtArea(path[posIdx].y - 1, path[posIdx].x - 1, path[posIdx].y + 1, path[posIdx].x + 1, true);
    
    console.log('Discovered area: ' + JSON.stringify(objects))
    
    var buildPos;
    for (let idx in objects)
    {
        var cellInfo = objects[idx];
        if (   (cellInfo.x === path[posIdx].x && cellInfo.y === path[posIdx].y)
            || (cellInfo.x === path[posIdx - 1].x && cellInfo.y === path[posIdx - 1].y)
            || (cellInfo.x === path[posIdx + 1].x && cellInfo.y === path[posIdx + 1].y)
        )
            continue;
        
        if (cellInfo.terrain !== 'wall')
            buildPos = cellInfo;
    }
    
    console.log('Container will be built at : ' + JSON.stringify(buildPos));
    
    room.createConstructionSite(buildPos.x, buildPos.y, STRUCTURE_CONTAINER);
    harvester.addAction(actions.CREEP_REMOTEHARVESTER_BUILDCONTAINER, {targetRoom: actionInfo.roomName});
    actionInfo.stage = 'startHarvest';
    // actionInfo.builderId = builder.id;
    
},

startHarvest : function(actionInfo, strategy)
{
    var harvester = _.filter(Game.creeps, (c) => c.id === actionInfo.harvesterCreepId)[0];
    
    harvester.addAction(actions.CREEP_HARVEST, {roomToHarvest: actionInfo.roomName, roomToCollect: actionInfo.roomName});
    actionInfo.stage = 'setupLorry';
//    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
//    
//    firstRoom.addSpawnAction(actions.SPAWN_REMOTEBUILDER, {});
//    actionInfo.stage = 'buildContainer';
},
    
setupLorry : function(actionInfo, strategy)
{
    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
    
    // firstRoom.addSpawnAction(actions.SPAWN_LORRY, {roomToCollect: actionInfo.roomName, roomToStore: actionInfo.fromRoomName});
    actionInfo.stage = 'waitSupplyCreeps';
},
    
waitSupplyCreeps : function(actionInfo, strategy)
{
    actionInfo.stage = 'manageSupplyCreeps';
    // var harvester = _.filter(Game.creeps, (c) => c.id === actionInfo.creepId)[0];
},

findCreeps : function(harvesterId, builderId, lorryId)
{
    console.log('Creeps to find: "' + harvesterId + '", "' + builderId + '", "' + lorryId + '"');
    var creeps = _.filter(Game.creeps, (c) => 
       ((harvesterId && c.id === harvesterId) || c.memory.roleId === creepsUtilities.roles.RemoteHarvester)
    || ((builderId && c.id === builderId) || c.memory.roleId === creepsUtilities.roles.RemoteBuilder)
    || ((lorryId && c.id === lorryId) || c.memory.roleId === creepsUtilities.roles.Lorry)
    );
    
    console.log('>>> temporary found creeps: ' + creeps);
    
    var result = {
        harvester : {
            designated : undefined,
            unused : undefined
        },
        builder : {
            designated : undefined,
            unused : undefined
        },
        lorry : {
            designated : undefined,
            unused : undefined
        },
    };
    
    if (!creeps && creeps.length == 0)    
        return result;

    for (var cIdx in creeps)
    {
        var creep = creeps[cIdx];
        if (harvesterId && creep.id === harvesterId)
            result.harvester.designated = creep;
        else if (!result.harvester.designated && creep.memory.roleId === creepsUtilities.roles.RemoteHarvester)
            result.harvester.unused = creep;
        else if (builderId && creep.id === builderId)
            result.builder.designated = creep;
        else if (!result.builder.designated && creep.memory.roleId === creepsUtilities.roles.RemoteBuilder)
            result.builder.unused = creep;
        else if (lorryId && creep.id === lorryId)
            result.lorry.designated = creep;
        else if (!result.lorry.designated && creep.memory.roleId === creepsUtilities.roles.Lorry)
            result.lorry.unused = creep;
    }
    
    return result;
},

manageSupplyCreeps : function(actionInfo, strategy)
{
    var firstRoom = Game.rooms[Memory.myRooms[0].roomName];
    // firstRoom.addSpawnAction(actions.SPAWN_LORRY, {roomToCollect: actionInfo.roomName, roomToStore: actionInfo.fromRoomName});
    
    if (!actionInfo.harvesterCreepId)
        actionInfo.harvesterCreepId = actionInfo.creepId;
    
    if (!actionInfo.builderCreepId)
        actionInfo.builderCreepId = actionInfo.builderId;
    
    var creeps = this.findCreeps(actionInfo.harvesterCreepId, actionInfo.builderCreepId, actionInfo.lorryCreepId);
    
    console.log("Found supply creeps: " + JSON.stringify(creeps));
    
    var harvester = creeps.harvester.designated;
    var builder = creeps.builder.designated;
    var lorry = creeps.lorry.designated;
    
    if (Memory.tmpRespawHarvester == undefined)
        Memory.tmpRespawHarvester = true;
    
    if (Memory.tmpRespawHarvester == true)
    {
        actionInfo.harvesterIsSpawning = false;
        Memory.tmpRespawHarvester = false;
    }
    
    if (!harvester)
    {
        if (creeps.harvester.unused)
        {
            harvester = creeps.harvester.unused;
            actionInfo.harvesterCreepId = harvester.id;
            actionInfo.harvesterIsSpawning = false;
            harvester.addAction(actions.CREEP_REMOTEHARVESTER_HARVEST, {});
        }
        else if (!actionInfo.harvesterIsSpawning)
        {
            firstRoom.addSpawnAction(actions.SPAWN_REMOTEHARVESTER, {roomToHarvest: actionInfo.roomName, roomToStore: actionInfo.roomName});
            actionInfo.harvesterIsSpawning = true;
        }
    }
    
    if (harvester && !harvester.memory.roomToHarvest)
    {
        harvester.memory.roomToHarvest = actionInfo.roomName;
        harvester.memory.roomToStore = actionInfo.roomName;
    }
    
    if (!builder)
    {
        if (creeps.builder.unused)
        {
            builder = creeps.builder.unused;
            actionInfo.builderCreepId = builder.id;
            actionInfo.builderIsSpawning = false;
            builder.addAction(actions.CREEP_REMOTEBUILDER_BUILD, {});
        }
        else if (!actionInfo.builderIsSpawning)
        {
            firstRoom.addSpawnAction(actions.SPAWN_REMOTEBUILDER, {roomToBuild: actionInfo.roomName});
            actionInfo.builderIsSpawning = true;
        }
    }
    
    if (builder && !builder.memory.roomToBuild)
        builder.memory.roomToBuild = actionInfo.roomName;
    
    if (!lorry)
    {
        if (creeps.lorry.unused)
        {
            lorry = creeps.lorry.unused;
            actionInfo.lorryCreepId = lorry.id;
            actionInfo.lorryIsSpawning = false;
            lorry.addAction(actions.CREEP_LORRY_TRANSPORTGOODS, {});
        }
        else if (!actionInfo.lorryIsSpawning)
        {
            firstRoom.addSpawnAction(actions.SPAWN_LORRY, {roomToCollect: actionInfo.roomName, roomToStore: actionInfo.fromRoomName});
            actionInfo.lorryIsSpawning = true;
        }
    }
    
},

doAction : function(actionInfo, strategy)
{
    if (!Memory.roomInUse)
        Memory.roomInUse = [];
    
    if (!actionInfo.stage)
        this.init(actionInfo);

    if (this[actionInfo.stage])
        this[actionInfo.stage](actionInfo, strategy);
    else
        console.log('GAME_USEROOM - undefined action stage: \'' + actionInfo.stage + '\'');
    
}
};

module.exports = gameActionImpl;

