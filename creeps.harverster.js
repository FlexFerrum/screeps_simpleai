var actions = require('gameaction');
var creepDefaultActions = require('creep.defaultactions')

var creepsHarvester = {
    initHarvesting : function (creep, actionInfo)
    {
        if (!creep.memory.isWorking)
            actionInfo.stage = 'harvestEnergy';
        else
            actionInfo.stage = 'storeEnergy';
    },
    
    harvestEnergy : function (creep, actionInfo)
    {
        if (creep.carry.energy == creep.carryCapacity)
        {
            actionInfo.stage = 'storeEnergy';
            creepDefaultActions.clearActionDefaultTarget(actionInfo);
            this.storeEnergy(creep, actionInfo);
            return;
        }
        
        var getFromContainer = creepDefaultActions.makeLoadEnergyAction(creep.pos, actionInfo.roomToCollect);
        var getFromPowerSource = creepDefaultActions.makeHarvestEnergyAction(creep.pos, actionInfo.roomToHarvest);
        
        var actions;
        
        if (Game.rooms[actionInfo.roomToCollect].energyCapacityAvailable != 0
         && Game.rooms[actionInfo.roomToCollect].energyAvailable < (Game.rooms[actionInfo.roomToCollect].energyCapacityAvailable) / 2)
        {
            actions = [getFromContainer, getFromPowerSource];
        }
        else
        {
            actions = [getFromPowerSource, getFromContainer];
        }
        
        var res = creepDefaultActions.executeOnTarget(actions, creep, actionInfo, {
            actionArg1 : RESOURCE_ENERGY,
            // doTrace : true,
            isCompleted : (s, a) => creepDefaultActions.getAvailableEnergy(s) == 0
        });
        
        return;
    },
    
    storeEnergy : function (creep, actionInfo)
    {
        if (creep.carry.energy == 0)
        {
            actionInfo.stage = 'harvestEnergy';
            creepDefaultActions.clearActionDefaultTarget(actionInfo);
            this.harvestEnergy(creep, actionInfo);
            return;
        }
        
        var startPos = creep.pos;
        if (creep.pos.roomName != actionInfo.roomToCollect)
            startPos = room.spawns[0].pos;
        
        var feedSpawn = creepDefaultActions.makeFeedSpawnAction(startPos, actionInfo.roomToCollect);
        var feedTower = creepDefaultActions.makeFeedTowerAction(startPos, actionInfo.roomToCollect);
        var storeEnergy = creepDefaultActions.makeStoreEnergyAction(startPos, actionInfo.roomToCollect);
        var buildNearest = creepDefaultActions.makeBuildNearestAction(startPos, actionInfo.roomToCollect);
        var upgradeAction = creepDefaultActions.makeUpgradeControllerAction(actionInfo.roomToCollect);
        
        creepDefaultActions.executeOnTarget([feedSpawn, feedTower, storeEnergy, buildNearest, upgradeAction], creep, actionInfo, {
            actionArg1 : RESOURCE_ENERGY,
            isCompleted : (s, a) => (s.store && s.store[RESOURCE_ENERGY] == s.storeCapacity) || (s.energy != undefined && s.energy == s.energyCapacity)
        });
    },
    
    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_GENERAL_HARVEST:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'initHarvesting';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },

    /** @param {Creep} creep **/
    run: function (creep)
    {
        if (!creep.memory.actionsQueue)
        {
            creep.addAction(actions.CREEP_GENERAL_HARVEST, {roomToHarvest: creep.memory.homeRoom, roomToCollect: creep.memory.homeRoom});
        }
        
        creepDefaultActions.defaultRun(creep, this, 800);

    }
};

module.exports = creepsHarvester;