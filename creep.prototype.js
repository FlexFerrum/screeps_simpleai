'use strict';

var creepsUtilities = require('creeps.utilities');
var creepsHarvester = require('creeps.harverster');
var creepsUpgrader = require('creeps.updater');
var creepsRepairer = require('creeps.repearer');
var creepsClaimer = require('creeps.claimer');
var creepsScout = require('creeps.scout');
var creepsBuilder = require('creeps.builder');
var creepsDefender = require('creeps.defender');
var creepsRemote = require('creeps.remote');
var creepsLorry = require('creeps.lorry');
var actions = require('gameaction');

var roleProcessors = [
    undefined,
    creepsHarvester,
    creepsUpgrader,
    creepsRepairer,
    creepsClaimer,
    creepsScout,
    creepsBuilder,
    creepsDefender,
    creepsRemote.remoteHarvester,
    creepsRemote.remoteBuilder,
    creepsLorry
];

Creep.prototype.homeRoom = function ()
{
    if (this.memory.homeRoom == undefined)
        return undefined;

    return Game.rooms[this.memory.homeRoom];
}

Creep.prototype.processCreepActivity = function()
{
    this.handleMove();
    roleProcessors[this.memory.roleId].run(this);
}

Creep.prototype.handleMove = function()
{
    if (this.memory.prevPos != undefined)
    {
        if (this.memory.prevPos.x == this.pos.x && this.memory.prevPos.y == this.pos.y)
            return;
    }
    
    this.memory.prevPos = this.pos;
    this.homeRoom().usePosition(this.pos);
}

Creep.prototype.moveToTarget = function(target)
{
    if (this.fatigue > 0)
        return false;
    
    this.moveTo(target);
    return true;
}

Creep.prototype.tryRenewCreep = function(timeToRenew)
{
    if (this.memory.energyUsedForCreation == undefined)
        return false;
    
    if (this.homeRoom().energyCapacityAvailable > this.memory.energyUsedForCreation)
        return false;
    
    if (!timeToRenew)
        timeToRenew = 200;
    
    if (!this.memory.isRenewing)
    {
        if (this.ticksToLive >= timeToRenew)
            return false;
    }
    
    if (this.ticksToLive >= timeToRenew * 2 &&
        this.homeRoom().energyAvailable <= this.homeRoom().energyCapacityAvailable / 2.5)
    {
        this.memory.isRenewing = false;
        return false;
    }
//    else
//        if ()
    
    var spawn = this.homeRoom().find(FIND_MY_SPAWNS)[0];
    if (spawn.spawning)
        return false;
    
    var renewResult = spawn.renewCreep(this);
    
    switch (renewResult)
    {
    case OK:
        this.memory.isRenewing = true;
        return true;
    case ERR_NOT_IN_RANGE:
        this.moveTo(spawn);
        return true;
    case ERR_FULL:
        this.memory.isRenewing = false;
        return false;
    case ERR_NOT_ENOUGH_ENERGY:
        this.memory.isRenewing = false;
        return false;
    }
    
    return false;
}

Creep.prototype.addAction = function(actionId, actionParams)
{
    if (!this.memory.actionsQueue)
        this.memory.actionsQueue = {queueId : 'creep-' + this.id};

    actionParams.actionId = actionId;
    actions.pushAction(this.memory.actionsQueue, actionParams);
}

Object.defineProperty(Creep.prototype, 'roleId', {
  get: function() {
    return this.memory.roleId;
  },
  configurable: false,
  enumerable: false
});

Object.defineProperty(Creep.prototype, 'isWorking', {
  get: function() {
    return !(!this.memory.isWorking);
  },
  set: function(v) {
    this.memory.isWorking = v;
  },
  configurable: false,
  enumerable: false
});
