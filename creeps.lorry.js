var actions = require('gameaction');
var creepsUtilities = require('creeps.utilities');
var creepDefaultActions = require('creep.defaultactions')

var creepsLorry = {
    doRemoteBuilding : function (creep, actionInfo)
    {
        actionInfo.stage = 'doGoodsTransferring';        
    },
                
    doGoodsTransferring : function (creep, actionInfo)
    {
        // creepsUtilities.checkWorkingMode(creep);
        
        console.log('>>>> LORRY step #1');
        if (creep.carry.energy == creep.carryCapacity)
            creep.memory.isWorking = true;

        if (!creep.memory.isWorking)
        {
            console.log('>>>> LORRY step #2');
            if (creep.pos.roomName !== creep.memory.roomToCollect)
            {
                console.log('>>>> LORRY step #3');
                creep.moveTo(new RoomPosition(25, 25, creep.memory.roomToCollect));
                return;
            }
               
            var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => s.structureType == STRUCTURE_CONTAINER
                             && s.store[RESOURCE_ENERGY] > 0
                             && s.pos.roomName === creep.memory.roomToCollect
                    });
                    
            console.log('>>>> LORRY step #4 ' + target);
            if (target)
            {
                console.log('>>>> LORRY step #5');
                if (target.transfer(creep, RESOURCE_ENERGY) === ERR_NOT_IN_RANGE)
                {
                    console.log('>>>> LORRY step #6 ');
                    creep.moveTo(target);
                }
                return;
            }
            
            target = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY);
            if (target)
            {
                if (creep.pickup(target) == ERR_NOT_IN_RANGE)
                    creep.moveTo(target);
                return;
            }
            
            creep.memory.isWorking = true;
        }
        else
        {
            console.log('>>>> LORRY step #7.0. Energy: ' + creep.carry.energy);
            if (creep.carry.energy != 0)
                console.log('>>>> LORRY step #7.1 ' + creepsUtilities.collectEnergy(creep, true));
            else
                creep.memory.isWorking = false;
        }
    },
    
    
    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_LORRY_TRANSPORTGOODS:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'doGoodsTransferring';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },
    
    run: function (creep)
    {
        creepDefaultActions.defaultRun(creep, this, 350);
    }
};

module.exports = creepsLorry;


