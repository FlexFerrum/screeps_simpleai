var actions = require('gameaction');
var gameActionDiscrover = require('game.actions.discover');
var gameActionUse = require('game.actions.use');

var gameStrategy = {
    
addGameAction : function(actionInfo)
{
    if (!Memory.actionsQueue)
        Memory.actionsQueue = {queueId : 'game-level'};

    actions.pushAction(Memory.actionsQueue, actionInfo);
},

findNextRoomToUse : function(room)
{
//    if (room == undefined)
//        return false;
    
    var neighbourRooms = Game.map.describeExits(room.name);
    
    var canUse = function(roomInfo, nameIdx)
    {
        var roomName = neighbourRooms[nameIdx];
        if (!roomName)
        {
            roomInfo.type = 'disconnected';
            return false;
        }
        
        console.log('Try neighbour room "' + roomName + '"');
        
        roomInfo.roomName = roomName;
        
        var room = Game.rooms[roomName];
        
        if (!room)
        {
            roomInfo.type = 'undiscovered';
            return true;
        }
        
        var roomController = room.controller;
        if (roomController.my)
        {
            roomInfo.type = 'controlled';
            return false;
        }
        
        if (roomController.owner !== undefined)
        {
            roomInfo.type = 'hostile';
            return false;
        }

        if (_.filter(Memory.roomInUse, (r) => r === roomName).length !== 0)
        {
            roomInfo.type = 'used';
            return false;
        }

        if (_.filter(Memory.roomsToDiscover, (r) => r === roomName).length !== 0)
        {
            roomInfo.type = 'discovering';
            return false;
        }
        
        roomInfo.type = 'neutral';
        return true;
    };
    
    var useRoom = function(memory, direction)
    {
        var roomInfo = memory[direction];
        var actionInfo = {
            actionId : actions.GAME_DISCOVERROOM,
            roomName : roomInfo.roomName,
            fromRoomName : room.name,
            direction : direction
        };
        
        gameStrategy.addGameAction(actionInfo);
    };
    
    if (!room.memory.leftRoom || room.memory.leftRoom.type === 'undiscovered')
    {
        if (!room.memory.leftRoom)
            room.memory.leftRoom = {};
        
        if (canUse(room.memory.leftRoom, '7'))
        {
            useRoom(room.memory, 'leftRoom');
            return true;
        }
    }
    
    if (!room.memory.topRoom || room.memory.topRoom.type === 'undiscovered')
    {
        if (!room.memory.topRoom)
            room.memory.topRoom = {};
        
        if (canUse(room.memory.topRoom, '1'))
        {
            useRoom(room.memory, 'topRoom');
            return true;
        }
    }
    
    if (!room.memory.rightRoom || room.memory.rightRoom.type === 'undiscovered')
    {
        if (!room.memory.rightRoom)
            room.memory.rightRoom = {};
        
        if (canUse(room.memory.rightRoom, '3'))
        {
            useRoom(room.memory, 'rightRoom');
            return true;
        }
    }
        
    if (!room.memory.bottomRoom || room.memory.bottomRoom.type === 'undiscovered')
    {
        if (!room.memory.bottomRoom)
            room.memory.bottomRoom = {};
        
        if (canUse(room.memory.bottomRoom, '5'))
        {
            useRoom(room.memory, 'bottomRoom');
            return;
        }
    }
},

findNewRoomToUse : function(roomsInUse)
{
    var roomsToCheck = _.filter(Memory.myRooms, (r) => 
            (!Game.rooms[r.roomName].memory.leftRoom || Game.rooms[r.roomName].memory.leftRoom.type === 'undiscovered')
         || (!Game.rooms[r.roomName].memory.topRoom || Game.rooms[r.roomName].memory.topRoom.type === 'undiscovered')
         || (!Game.rooms[r.roomName].memory.rightRoom || Game.rooms[r.roomName].memory.rightRoom.type === 'undiscovered')
         || (!Game.rooms[r.roomName].memory.bottomRoom || Game.rooms[r.roomName].memory.bottomRoom.type === 'undiscovered')
    );
    
    for (var rIdx in roomsToCheck)
    {
        var r = roomsToCheck[rIdx];
        console.log("Trying to check room: " + JSON.stringify(r));
    
        if (this.findNextRoomToUse(Game.rooms[roomsToCheck[rIdx].roomName]))
            break;
    }
},

doExpansion : function()
{
    console.log('Known rooms count: ' + JSON.stringify(Game.rooms));
    
//    delete Memory.myRooms;
//    Memory.myRooms = undefined;
    
    if (Memory.myRooms == undefined)
    {
        var firstRoom = Game.rooms[Object.keys(Game.rooms)[0]].name;
        console.log('First room is "' + firstRoom + '"');
        Memory.myRooms = [];
        Memory.myRooms.push({roomName: firstRoom, state: 'controlled', homeRoom: true});
        Memory.roomsInUse = [];
        Memory.roomsInUse.push(firstRoom);
    }
    
    var useRoomActions = _.filter(Memory.actionsInProgress, (a) => a.actionId === actions.GAME_DISCOVERROOM);
    if (useRoomActions.length == 0)
    {
        this.findNewRoomToUse(Memory.roomsInUse);
    }
},

processCurrentActions : function(currentActions)
{
    for (var idx in currentActions)
    {
        var a = currentActions[idx];
        
        switch (a.actionId)
        {
            case actions.GAME_DISCOVERROOM:
                gameActionDiscrover.doAction(a, gameStrategy);
                break;
            case actions.GAME_USEROOM:
                gameActionUse.doAction(a, gameStrategy);
                break;
            default:
                console.log('!!!!!! Unkown action: ' + actions.toString(a));
                break;
        };
    }
},

runStrategy : function()
{
    /*
    delete Memory.actionsInProgress;
    delete Memory.actionsQueue;
    delete Memory.roomsToDiscover;

    Memory.actionsInProgress = [];
    Memory.roomsToDiscover = undefined;
    Memory.actionsQueue = undefined;
    Memory.actionId = 0;
    Memory.keepActions = true;
    return;
    */
    this.doExpansion();
    
    if (!Memory.actionsInProgress)
        Memory.actionsInProgress = [];
    
    actions.processQueue(Memory.actionsQueue, Memory.actionsInProgress, 5);
    this.processCurrentActions(Memory.actionsInProgress);
    actions.removeFinishedActions(Memory.actionsInProgress);
}

};

module.exports = gameStrategy;

