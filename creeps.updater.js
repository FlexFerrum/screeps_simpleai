var actions = require('gameaction');
var creepDefaultActions = require('creep.defaultactions')

var creepsUpgrader = {
    initUpgrading : function (creep, actionInfo)
    {
        if (!creep.memory.isWorking)
            actionInfo.stage = 'getEnergy';
        else
            actionInfo.stage = 'doUpgrade';
    },
    
    getEnergy : function (creep, actionInfo)
    {
        var res = creepDefaultActions.getEnergy(creep, actionInfo, actionInfo.roomToUpgrade, this);
        if (res == creepDefaultActions.results.ACTION_IN_PROGRESS)
            return;
        
        if (res == creepDefaultActions.results.ACTION_DONE)
        {
            actionInfo.stage = 'doUpgrade';
            creepDefaultActions.clearActionDefaultTarget(actionInfo);
            this.doUpgrade(creep, actionInfo);
        }
        // TODO: Do something else
        return;
    },

    doUpgrade : function (creep, actionInfo)
    {
        if (creep.carry.energy == 0)
        {
            actionInfo.stage = 'getEnergy';
            creepDefaultActions.clearActionDefaultTarget(actionInfo);
            this.getEnergy(creep, actionInfo);
            return;
        }
        
        creepDefaultActions.executeOnTarget([creepDefaultActions.makeUpgradeControllerAction(actionInfo.roomToUpgrade)], creep, actionInfo, {
            actionArg1 : RESOURCE_ENERGY,
            doTrace : false
        });
    },

    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_GENERAL_UPGRADECONTROLLER:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'initUpgrading';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },

    /** @param {Creep} creep **/
    run: function (creep, spawn)
    {
        if (!creep.memory.actionsQueue)
        {
            creep.addAction(actions.CREEP_GENERAL_UPGRADECONTROLLER, {roomToUpgrade: creep.memory.homeRoom});
        }
        
        creepDefaultActions.defaultRun(creep, this, 800);
    }
};

module.exports = creepsUpgrader;