Room.prototype.usePosition = function (pos)
{
    var posName = 'pos_' + pos.x + '_' + pos.y;
    
//    if (this.memory.usedPositions)
//    {
//        delete this.memory.usedPositions;
//        this.memory.usedPositions = undefined;
//    }
    
    var posInfo;
    if (this.memory.usedPositions)
        posInfo = this.memory.usedPositions[posName];
    
    if (!posInfo)
    {
        posInfo = {
            x : pos.x,
            y : pos.y,
            used : 1
        };
        
        if (!this.memory.usedPositions)
            this.memory.usedPositions = {};
        
        this.memory.usedPositions[posName] = posInfo;
    }
    else
        posInfo.used ++;
}

Room.prototype.defend = function ()
{
    var towers = this.find(FIND_MY_STRUCTURES, {
            filter: { structureType: STRUCTURE_TOWER }
        });
        
    if (towers.length == 0)
        return;
    
    for (var idx in towers)
    {
        var tower = towers[idx];
        var target = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (target)
        {
            tower.attack(target);
            continue;
        }
        
        target = tower.pos.findClosestByRange(FIND_MY_CREEPS, (creep) => creep.hits < creep.hitsMax);
        if (target)
        {
            tower.heal(target);
            continue;
        }
        
        target = tower.pos.findClosestByRange(FIND_MY_STRUCTURES, (s) => s.hits < s.hitsMax);
        if (target)
        {
            tower.repair(target);
            continue;
        }
    }
}

Room.prototype.run = function()
{
    _.each(this.find(FIND_MY_STRUCTURES, {filter : (s) => s.processQueue !== undefined}), (s) => s.processQueue());
}

Room.prototype.addSpawnAction = function(actionId, actionParams)
{
    var mainSpawn = this.spawns[0];
    
    mainSpawn.addAction(actionId, actionParams);
}

Object.defineProperty(Room.prototype, 'spawns', {
    get: function ()
    {
        var result = [];
        var spawns = this.find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_SPAWN}});
        for (var idx in spawns)
            result.push(spawns[idx]);
        
        return result;
    },
  configurable: false,
  enumerable: false
});

