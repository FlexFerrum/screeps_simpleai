var creepsUtilities = require('creeps.utilities');
var creepsHarvester = require('creeps.harverster');
var creepsUpgrader = require('creeps.updater');
var creepsRepairer = require('creeps.repearer');
var creepsClaimer = require('creeps.claimer');
var creepsScout = require('creeps.scout');
var creepsBuilder = require('creeps.builder');
var creepsDefender = require('creeps.defender');

var harvestCreep = creepsHarvester.run;
var upgraderCreep = creepsUpgrader.run;
var repairerCreep = creepsRepairer.run;
var claimerCreep = creepsClaimer.run;
var scoutCreep = creepsScout.run;
var defenderCreep = creepsDefender.run;
var builderCreep = creepsBuilder.run;

var creepsManagerImpl = {
    // repearerCreep : creepsRepearer.run,
    // claimingCreep : creepsClaimer.run,

    calculateEnery: function (spawn)
    {
        var curEnergy = spawn.room.energyAvailable;
        var maxEnergy = spawn.room.energyCapacityAvailable;

        console.log('Available energy: ' + curEnergy + ', maxEnery: ' + maxEnergy);

        return {curEnergy: curEnergy, maxEnergy: maxEnergy};
    },
    
    getHarvesterConfig: function(energy)
    {
        var numberOfParts = Math.floor(energy / 200);
        // make sure the creep is not too big (more than 50 parts)
        numberOfParts = Math.min(numberOfParts, Math.floor(50 / 3));
        var body = [];
        for (let i = 0; i < numberOfParts; i++) {
            body.push(WORK);
        }
        for (let i = 0; i < numberOfParts; i++) {
            body.push(CARRY);
        }
        for (let i = 0; i < numberOfParts; i++) {
            body.push(MOVE);
        }
        
        return body;
    },

    createWorkingCreeps: function (spawn, roleId, creepsCap)
    {
        var myCreeps = _.filter(Game.creeps, (creep) => creep.my && creep.memory.roleId < 4); // Ugly!!!
        var creeps = _.filter(myCreeps, (creep) => creep.memory.roleId === roleId);
        console.log('Creeps of role ' + creepsUtilities.roleName(roleId) + ': ' + creeps.length);

        if (creeps.length >= creepsCap)
            return;

        if (spawn.spawning)
            return;

        var e = creepsManagerImpl.calculateEnery(spawn);
        var curEnergy = e.curEnergy;
        var maxEnergy = e.maxEnergy;
        var energyToUse = maxEnergy;
        
        if (myCreeps.length < 2)
        {
            energyToUse = curEnergy;
            roleId = creepsUtilities.CreepRoleHarvester;
        }

        if (energyToUse < 200)
            return;

        var harvestingConfig = [WORK, CARRY, MOVE];
        var claimingConfig = undefined;

        if (energyToUse >= 350)
            harvestingConfig = [WORK, WORK, CARRY, CARRY, MOVE];

        if (energyToUse >= 400)
            harvestingConfig = [WORK, WORK, CARRY, CARRY, MOVE, MOVE];

        if (energyToUse >= 500)
            harvestingConfig = [WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE];
        
        if (energyToUse >= 600)
            harvestingConfig = this.getHarvesterConfig(energyToUse);
        
        if (energyToUse >= 700)
            claimingConfig = [MOVE, MOVE, CLAIM];

        var creepConfig = undefined;

        switch (roleId)
        {
            case creepsUtilities.CreepRoleHarvester:
                creepConfig = harvestingConfig;
                break;
            case creepsUtilities.CreepRoleUpgrader:
                creepConfig = harvestingConfig;
                break;
            case creepsUtilities.CreepRoleRepairer:
                creepConfig = harvestingConfig;
                break;
            case creepsUtilities.CreepRoleClaimer:
                creepConfig = claimingConfig;
                break;
        }

        var spawningResult = creepsUtilities.createCreep(spawn, creepConfig, roleId, {energyUsedForCreation: energyToUse});

        if (_.isString(spawningResult))
        {
            var newE = creepsManagerImpl.calculateEnery(spawn);
            console.log('New creep of "' + creepConfig + '" with role ' + creepsUtilities.roleName(roleId) + ' successfully spawned.');
            return true;
        }
        else
        {
            console.log('Spawn of [' + creepConfig + '] error: ' + spawningResult);
            return false;
        }
    },

    createDefender: function (spawn)
    {
        var myCreeps = _.filter(Game.creeps, (creep) => creep.my);
        var creeps = _.filter(myCreeps, (creep) => creep.memory.roleId === creepsUtilities.CreepRoleDefender);
        
        if (creeps.length >= 2)
            return;
        
        var e = this.calculateEnery(spawn);
        
        if (e.curEnergy < 450)
            return;
        
        var creepConfig = [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE];
        var usedEnergy = 450;
        
        if (e.maxEnergy >= 920)
        {
            creepConfig = [TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, TOUGH, ATTACK, ATTACK, ATTACK, ATTACK, MOVE, MOVE, MOVE, MOVE];
            usedEnergy = 920;
        }

        var spawningResult = creepsUtilities.createCreep(spawn, creepConfig, creepsUtilities.CreepRoleDefender, {energyUsedForCreation: usedEnergy});

        if (_.isString(spawningResult))
        {
            var newE = creepsManagerImpl.calculateEnery(spawn);
            console.log('New creep of "' + creepConfig + '" with role defender successfully spawned.');
            return true;
        }        
    }

};

var creepsManager = {
    /** @param {Creep} creep **/
    cleanupCreeps: function ()
    {
        for (var name in Memory.creeps)
        {
            if (!Game.creeps[name])
            {
                delete Memory.creeps[name];
                console.log('Clearing non-existing creep memory:', name);
            }
        }
    },

    createRemoteHarvester: function (spawn, extraParams)
    {
        var e = creepsManagerImpl.calculateEnery(spawn);
        
        if (e.curEnergy < 600)
            return false;
        
        harvestingConfig = creepsManagerImpl.getHarvesterConfig(e.maxEnergy);
        var memory = extraParams ? extraParams : {};
        memory.energyUsedForCreation = e.maxEnergy;
                
        var spawningResult = creepsUtilities.createCreep(spawn, harvestingConfig, creepsUtilities.roles.RemoteHarvester, memory);

        if (_.isString(spawningResult))
        {
            console.log('New creep of "' + harvestingConfig + '" with role Harvester successfully spawned.');
            return true;
        }
        
        return false;
    },

    createRemoteBuilder: function (spawn, extraParams)
    {
        var e = creepsManagerImpl.calculateEnery(spawn);
        
        if (e.curEnergy < 600)
            return false;
        
        harvestingConfig = creepsManagerImpl.getHarvesterConfig(e.maxEnergy);
        
        var memory = extraParams ? extraParams : {};
        memory.energyUsedForCreation = e.maxEnergy;
        
        var spawningResult = creepsUtilities.createCreep(spawn, harvestingConfig, creepsUtilities.roles.RemoteBuilder, memory);

        if (_.isString(spawningResult))
        {
            console.log('New creep of "' + harvestingConfig + '" with role RemoteBuilder successfully spawned.');
            return true;
        }
        
        return false;
    },

    createScout: function (spawn, extraParams)
    {
        var e = creepsManagerImpl.calculateEnery(spawn);
        
        if (e.curEnergy < 500)
            return false;
        
        var creepConfig = [MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE, MOVE];

        var memory = extraParams ? extraParams : {};
        memory.energyUsedForCreation = e.maxEnergy;
        
        var spawningResult = creepsUtilities.createCreep(spawn, creepConfig, creepsUtilities.CreepRoleScout, memory);

        if (_.isString(spawningResult))
        {
            console.log('New creep of "' + creepConfig + '" with role scout successfully spawned.');
            return true;
        }
        
        return false;
    },

    createLorry: function (spawn, extraParams)
    {
        var e = creepsManagerImpl.calculateEnery(spawn);
        
        if (e.curEnergy < 650)
            return false;
        
        var creepConfig = [MOVE, MOVE, MOVE, MOVE, MOVE, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY];
        if (e.curEnergy >= 1500)
            creepConfig = [MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY, MOVE, CARRY, CARRY];
        
        var memory = extraParams ? extraParams : {};
        memory.energyUsedForCreation = e.maxEnergy;        

        var spawningResult = creepsUtilities.createCreep(spawn, creepConfig, creepsUtilities.roles.Lorry, memory);

        if (_.isString(spawningResult))
        {
            console.log('New creep of "' + creepConfig + '" with role Lorry successfully spawned.');
            return true;
        }
        
        return false;
    },

    spawnCreeps: function (spawn)
    {
        if (!creepsManagerImpl.createWorkingCreeps(spawn, creepsUtilities.CreepRoleHarvester, 3))
            if (!creepsManagerImpl.createWorkingCreeps(spawn, creepsUtilities.CreepRoleUpgrader, 2))
                if (!creepsManagerImpl.createWorkingCreeps(spawn, creepsUtilities.CreepRoleRepairer, 2))
                    creepsManagerImpl.createDefender(spawn);

        // creepsManagerImpl.createScout(spawn);
    },

    runCreep: function (spawn, creep)
    {
        if (creep.memory.homeRoom == undefined)
            creep.memory.homeRoom = spawn.room.name;
        
        if (creep.memory.justInitialized === undefined || creep.memory.justInitialized)
        {
            creepsUtilities.initCreep(creep);
            return;
        }
        
        if (creep.spawning)
            return;

        creep.processCreepActivity();
    }
}

module.exports = creepsManager;