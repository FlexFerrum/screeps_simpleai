var actions = require('gameaction');
var creepsUtilities = require('creeps.utilities');
var creepDefaultActions = require('creep.defaultactions')

var creepsRemoteHarvester = {
    storeEnergy : function (creep)
    {
        var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_CONTAINER
                         && s.store[RESOURCE_ENERGY] < s.storeCapacity
                         && s.pos.roomName === creep.pos.roomName
                });
                
        if (!target)
            return false;
        
        if (creep.transfer(target, RESOURCE_ENERGY) !== OK)
            creep.moveTo(target);
        
        return true;
    },
    
    findRoomEntrance : function (creep, actionInfo)
    {
        console.log('>>>> Flags: ' + JSON.stringify(Game.flags));
        var flags = _.filter(Game.flags, (flag) => flag.color == COLOR_WHITE && flag.pos.roomName === actionInfo.targetRoom);
        if (flags.length == 0)
        {
            console.log('!!!!!!!! No white flag set for entrance into room ' + actionInfo.targetRoom + '!');
            return;
        }
        
        actionInfo.targetPosition = {x : flags[0].pos.x, y : flags[0].pos.y};
        actionInfo.sourcePosition = {x : creep.pos.x, y : creep.pos.y};
        actionInfo.stage = 'moveToTarget';
    },
    
    moveToTarget : function (creep, actionInfo)
    {
        if (creep.pos.roomName === actionInfo.targetRoom)
        {
            if (creep.pos.isEqualTo(actionInfo.targetPosition.x, actionInfo.targetPosition.y))
            {
                actionInfo.state = actions.state.Finished;
                return;
            }
        }
        
        creep.moveTo(new RoomPosition(actionInfo.targetPosition.x, actionInfo.targetPosition.y, actionInfo.targetRoom));
    },
    
    buildContainer : function (creep, actionInfo)
    {
        creepsUtilities.checkWorkingMode(creep);
        var roomName = actionInfo.targetRoom;
        if (!roomName)
            roomName = creep.memory.roomToHarvest;
        
        if (creep.pos.roomName !== roomName)
        {
            creep.moveTo(new RoomPosition(25, 25, roomName));
            return;
        }

        if (!creep.memory.isWorking)
        {
            creepsUtilities.harvestEnergy(creep, roomName);
            return;
        }
        
        var target = creep.pos.findClosestByPath(FIND_CONSTRUCTION_SITES, {
            filter: (s) => s.structureType === STRUCTURE_CONTAINER
                });
                                
        if (target)
            creepsUtilities.buildSite(creep, target);
        else
            actionInfo.stage = 'harvestEnergy';
    },
    
    harvestEnergy : function (creep, actionInfo)
    {
        creepsUtilities.checkWorkingMode(creep);
        
        if (creep.pos.roomName !== creep.memory.roomToHarvest)
        {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.roomToHarvest));
            return;
        }
        
        var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s) => s.structureType == STRUCTURE_CONTAINER
                && s.pos.roomName === creep.pos.roomName
        });
        
        if (!target)
        {
            this.buildContainer(creep, actionInfo);
            return;
        }

        if (!creep.memory.isWorking)
            creepsUtilities.harvestEnergy(creep, creep.memory.roomToHarvest);
        else
            this.storeEnergy(creep);
    },
    
    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_REMOTEHARVESTER_FINDROOM:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'findRoomEntrance';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            case actions.CREEP_REMOTEHARVESTER_BUILDCONTAINER:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'buildContainer';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            case actions.CREEP_REMOTEHARVESTER_HARVEST:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'harvestEnergy';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },

    run: function (creep)
    {
        creepDefaultActions.defaultRun(creep, this, 350);
    }
};

var creepsRemoteBuilder = {
    doRemoteBuilding : function (creep, actionInfo)
    {
        creepsUtilities.checkWorkingMode(creep);
        
        if (creep.pos.roomName !== creep.memory.roomToBuild)
        {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.roomToBuild));
            return;
        }

        if (!creep.memory.isWorking)
        {
            creepsUtilities.harvestEnergy(creep, creep.memory.roomToBuild);
            return;
        }
        
        var target;
        target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES, {
            filter: (s) => s.pos.roomName === creep.memory.roomToBuild
                });
                
        // console.log(">>>>>>>>> REMOTE BUILDER #6 " + target);
        if (target)
        {
            creepsUtilities.buildSite(creep, target);
            return;
        }
        
        var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (s) => s.pos.roomName === creep.memory.roomToBuild
                && s.hits < s.hitsMax
                });
                
        if (target)
        {
            if (creep.repair(target) == ERR_NOT_IN_RANGE)
                creep.moveTo(target);

            return;
        }
        
        creepsRemoteHarvester.storeEnergy(creep);
    },
    
    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_REMOTEBUILDER_BUILD:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'doRemoteBuilding';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },

    run: function (creep)
    {
        creepDefaultActions.defaultRun(creep, this, 350);
    }    
};

module.exports = {
    remoteHarvester : creepsRemoteHarvester,
    remoteBuilder : creepsRemoteBuilder
};