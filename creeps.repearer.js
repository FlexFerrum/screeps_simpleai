var actions = require('gameaction');
var creepDefaultActions = require('creep.defaultactions')

var creepsRepairer = {
    initBuilding : function (creep, actionInfo)
    {
        if (!creep.memory.isWorking)
            actionInfo.stage = 'getEnergy';
        else
            actionInfo.stage = 'doBuilding';
    },
    
    getEnergy : function (creep, actionInfo)
    {
        var res = creepDefaultActions.getEnergy(creep, actionInfo, actionInfo.roomToBuild, this);
        
        if (res == creepDefaultActions.results.ACTION_IN_PROGRESS)
            return;
        
        if (res == creepDefaultActions.results.ACTION_DONE)
        {
            actionInfo.stage = 'doBuilding';
            this.doBuilding(creep, actionInfo);
        }
        // TODO: Do something else
        return;
    },

    doBuilding : function (creep, actionInfo)
    {
        if (creep.carry.energy == 0)
        {
            actionInfo.stage = 'getEnergy';
            actionInfo.currentTarget = undefined;
            this.getEnergy(creep, actionInfo);
            return;
        }
        
        var repairAction = creepDefaultActions.makeRepairNearestAction(creep.pos, actionInfo.roomToBuild);
        var buildAction = creepDefaultActions.makeBuildNearestAction(creep.pos, actionInfo.roomToBuild);
        var upgradeAction = creepDefaultActions.makeUpgradeControllerAction(actionInfo.roomToBuild);
        
        creepDefaultActions.executeOnTarget([repairAction, buildAction, upgradeAction], creep, actionInfo, {
            actionArg1 : RESOURCE_ENERGY,
            isCompleted : (s, a) => a === 'upgradeController' || (a === 'repair' && s.hits === s.hitsMax)
        });
    },

    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_GENERAL_BUILD:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'initBuilding';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('>>>>>> Unkown action: ' + actions.toString(actionInfo));
                break;
        };        
    },

    /** @param {Creep} creep **/
    run: function (creep)
    {
        if (!creep.memory.actionsQueue)
        {
            creep.addAction(actions.CREEP_GENERAL_BUILD, {roomToBuild: creep.memory.homeRoom});
        }
        
        creepDefaultActions.defaultRun(creep, this, 800);

    }
};

module.exports = creepsRepairer;