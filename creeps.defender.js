
var creepsDefender = {

    /** @param {Creep} creep **/
    run: function (creep)
    {
//        var hostileCreeps = creep.homeRoom().find(FIND_HOSTILE_CREEPS);
//
//        if (hostileCreeps.length() == 0)
//         return;
        if (creep.memory.roomToDefend && creep.pos.roomName !== creep.memory.roomToDefend)
        {
            creep.moveTo(new RoomPosition(25, 25, creep.memory.roomToDefend));
            return;
        }

        var target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (target)
        {
            if (creep.attack(target) == ERR_NOT_IN_RANGE)
            {
                creep.moveTo(target);
            }
        }
        else
            creep.tryRenewCreep();
        
//        var target = hostileCreeps[0];
//        if (this.attack(target) == )
    }
};

module.exports = creepsDefender;


