var creepsUtilities = require('creeps.utilities');

var creepsClaimer = {

    /** @param {Creep} creep **/
    run: function (creep)
    {
        if (creep.memory.targetRoom == undefined)
        {
            
        }
        
        // if in target room
        if (creep.room.name != creep.memory.targetRoom)
        {
            // find exit to target room
            var exit = creep.room.findExitTo(creep.memory.targetRoom);
            // move to exit
            creep.moveTo(creep.pos.findClosestByRange(exit));
        }
        else
        {
            // try to claim controller
            if (creep.claimController(creep.room.controller) == ERR_NOT_IN_RANGE)
            {
                // move towards the controller
                creep.moveTo(creep.room.controller);
            }
        }
    }
};

module.exports = creepsClaimer;