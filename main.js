/* global Game */

var creepsHarvester = require('creeps.harverster');
var creepsUpgrader = require('creeps.updater');
var creepsManager = require('creeps.manager');
var roomManager = require('room.manager');
var gameStrategy = require('game.strategy');

require('prototypes');

module.exports.loop = function () {
    var spawn = Game.spawns['MainSpawn'];
    
    creepsManager.cleanupCreeps();
    gameStrategy.runStrategy();
    
    creepsManager.spawnCreeps(spawn);
    roomManager.run(spawn);
    
    for (var idx in Game.rooms)
    {
        var room = Game.rooms[idx];
        room.run();
    }
    
    for(var name in Game.creeps) 
    {
        var creep = Game.creeps[name];
        
        if (creep.memory.roleId !== undefined)
        {
            creepsManager.runCreep(spawn, creep);
            continue;
        }
    }
};