var actions = require('gameaction');

var creepsScout = {
    findAdjustedRoomConnection : function (roomName, x, y, nextXFn, nextYFn)
    {
        var resultPos;
        for (; x != 50 || y != 50; x = nextXFn(x), y = nextYFn(y))
        {
            if (Game.map.getTerrainAt(x, y, roomName) != 'wall')
            {
                resultPos = {x : x, y : y};
                break;
            }
        }
        
        return resultPos;
    },
    
    findRoomToExplore: function (creep, startRoomId, targetRoomId, direction)
    {
        var room = Game.rooms[startRoomId];
        var coords = 
        // 00  01  02  03  04  05  06  07  08  09  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40  41  42  43  44  45  46  47  48  49
        [  50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10,  9,  8,  7,  6,  5,  4,  3,  2,  1,  0];
        
        var repeatFn = (x) => x;
        var changeFn = (x) => coords[x];
        
        var resultPos = {room: targetRoomId};
        var pos;
        
        // console.log('!!!!! scout-findRoomToExplore #1');
        switch (direction)
        {
        case 'leftRoom':
            pos = this.findAdjustedRoomConnection(startRoomId, 0, 25, repeatFn, changeFn);
            resultPos.x = 48;
            resultPos.y = pos.y;
            break;
        case 'topRoom':
            pos = this.findAdjustedRoomConnection(startRoomId, 25, 0, changeFn, repeatFn);
            resultPos.x = pos.x;
            resultPos.y = 48;
            break;
        case 'rightRoom':
            pos = this.findAdjustedRoomConnection(startRoomId, 49, 25, repeatFn, changeFn);
            resultPos.x = 1;
            resultPos.y = pos.y;
            break;
        case 'bottomRoom':
            pos = this.findAdjustedRoomConnection(startRoomId, 25, 49, changeFn, repeatFn);
            resultPos.x = pos.x;
            resultPos.y = 1;
            break;
        };
        
        return resultPos;
    },
    
    findTargetPosition : function (creep, actionInfo)
    {
        var resultPos = this.findRoomToExplore(creep, actionInfo.sourceRoom, actionInfo.targetRoom, actionInfo.sourceDirection);
        console.log('Found pos for discovering: ' + JSON.stringify(resultPos));
        actionInfo.targetPosition = resultPos;
        actionInfo.stage = 'moveToTarget';
    },
    
    moveToTarget : function (creep, actionInfo)
    {
        let targetPos = new RoomPosition(actionInfo.targetPosition.x, actionInfo.targetPosition.y, actionInfo.targetPosition.room);
        
        if (creep.pos.isEqualTo(targetPos))
        {
            actionInfo.state = actions.state.Finished;
            return;
        }
        
        creep.moveTo(targetPos);
    },
    
    processAction : function (creep, actionInfo)
    {
        switch (actionInfo.actionId)
        {
            case actions.CREEP_SCOUT_DISCOVER:
                if (actionInfo.state === actions.state.Created)
                {
                    actionInfo.state = actions.state.InProgress;
                    actionInfo.stage = 'findTargetPosition';
                }
                this[actionInfo.stage](creep, actionInfo);
                break;
            default:
                console.log('!!!!!! Unkown action: ' + actions.toString(a));
                break;
        };        
    },
    
    /** @param {Creep} creep **/
    run: function (creep)
    {
//        if (creep.tryRenewCreep())
//            return;
        
        actions.processQueueAndActions(creep.memory, (ai) => this.processAction(creep, ai), 1);    
        
        // creep.suicide();
        return;
        /*
        if (!creep.memory.targetPos || creep.pos.isEqualTo(creep.memory.targetPos.x, creep.memory.targetPos.y))
        {
            if (!creep.memory.startRoom)
                creep.memory.startRoom = creep.memory.homeRoom;

            console.log('Room exits: ' + JSON.stringify(Game.map.describeExits(creep.memory.homeRoom)));

            if (!creep.memory.targetRoom)
            {
                console.log('!!!!! scout-run #1');
                var pos = this.findRoomToExplore(creep, creep.memory.startRoom);
                if (pos)
                    console.log('!!!!! Next room exit found: ' + pos);
            }
            
            creep.moveTo(new RoomPosition(creep.memory.targetPos.x, creep.memory.targetPos.y, creep.memory.targetRoom));
        }
        */
    }
};

module.exports = creepsScout;
