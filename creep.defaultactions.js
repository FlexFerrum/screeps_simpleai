var actions = require('gameaction');
var creepsUtilities = require('creeps.utilities');

var creepDefaultActions = {
    results : 
    {
        OK : OK,
        ACTION_IN_PROGRESS : 1,
        ACTION_DONE : 2,
        NO_ENERGY : -100,
        NO_TARGET : -101,
        INVALID_ACTION : -102,
        NO_CAPACITY : -103
    },
    
    doSimpleAction : function(creep, target, actionId, extraArg1, extraArg2, extraArg3)
    {
        var res = this.results.INVALID_ACTION;
        
        switch (actionId)
        {
        case 'transferToCreep':
            if (target['transfer'] != undefined)
                res = target.transfer(creep, extraArg1);
            break;
        case 'transferFromCreep':
            actionId = 'transfer';
        default:
            if (creep[actionId] != undefined)
                res = creep[actionId](target, extraArg1, extraArg2, extraArg3);
            else
                console.log('!!!! Invalid simple actionId for creep: ' + actionId);
        }        
        
        return res;
    },
    
    clearActionDefaultTarget : function(actionInfo)
    {
        actionInfo.currentTarget = undefined;
        actionInfo.actionOnTarget = undefined;
    },
    
    makeRepairNearestAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => s.hits < (s.hitsMax * 0.9)
                    && (!roomName || s.pos.roomName === roomName)
            }),
            action : 'repair'
        };
    },
    
    makeBuildNearestAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_CONSTRUCTION_SITES, {
                filter: (s) => !roomName || s.pos.roomName === roomName
            }),                    
            action : 'build'
        };
    },
    
    makeUpgradeControllerAction : function(roomName)
    {
        return {
            locator : () => Game.rooms[roomName].controller,
            action : 'upgradeController'
        };
    },
    
    makeFeedSpawnAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (s) => (s.structureType == STRUCTURE_SPAWN || s.structureType == STRUCTURE_EXTENSION)
                             && s.energy < s.energyCapacity
                             && s.pos.roomName === roomName
            }),
            action : 'transferFromCreep'
        };
    },
    
    makeFeedTowerAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_MY_STRUCTURES, {
                filter: (s) => (s.structureType == STRUCTURE_TOWER)
                             && s.energy < s.energyCapacity
                             && s.pos.roomName === roomName
            }),
            action : 'transferFromCreep'
        };
    },
    
    makeStoreEnergyAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE)
                             && s.store[RESOURCE_ENERGY] < s.storeCapacity
                             && s.pos.roomName === roomName
            }),
            action : 'transferFromCreep'
        };
    },
    
    makeHarvestEnergyAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByPath(FIND_SOURCES_ACTIVE, {
                filter: (s) => s.pos.roomName === roomName
            }),
            action : 'harvest'
        };
    },
    
    makeLoadEnergyAction : function(pos, roomName)
    {
        return {
            locator : () => pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE)
                             && s.store[RESOURCE_ENERGY] != 0
                             && s.pos.roomName === roomName
            }),
            action : 'transferToCreep'
        };
    },
    
    executeOnTarget : function(actions, creep, actionInfo, extraParams)
    {
        var targetFieldName = extraParams.targetFieldName ? extraParams.targetFieldName : 'currentTarget';
        var actionFieldName = extraParams.actionFieldName ? extraParams.actionFieldName : 'actionOnTarget';
        
        var target;
        var actionOnTarget = actionInfo[actionFieldName];
        var targetId = actionInfo[targetFieldName];
        
        if (extraParams.doTrace)
            console.log('>>> executeOnTarget#1. actionOnTarget: ' + actionOnTarget + ', targetId: ' + targetId);
        
        if (targetId)
            target = Game.getObjectById(targetId);
        
        if (extraParams.doTrace)
            console.log('>>> executeOnTarget#2. actionOnTarget: ' + actionOnTarget + ', targetId: ' + targetId + ', target: ' + JSON.stringify(target));
        
        if (target && extraParams.isCompleted && extraParams.isCompleted(target, actionOnTarget))
        {
            target = undefined;
            actionInfo[targetFieldName] = undefined;
            actionInfo[actionFieldName] = undefined;
        }
        
        // No previously stored structure
        for (var aidx in actions)
        {
            var ai = actions[aidx];
            if (target)
                break;              

            target = ai.locator();
            if (target)
                actionOnTarget = ai.action;
            
            if (extraParams.doTrace)
                console.log('>>>>> ActionIdx: ' + aidx + ', found target: ' + target + ' for action ' + actionOnTarget);
        }

        if (!target)
        {
            actionInfo[targetFieldName] = undefined;
            actionInfo[actionFieldName] = undefined;
            return;
        }
        
        actionInfo[targetFieldName] = target.id;
        actionInfo[actionFieldName] = actionOnTarget;

        var res = creepDefaultActions.doSimpleAction(creep, target, actionOnTarget, extraParams.actionArg1, extraParams.actionArg2, extraParams.actionArg3, extraParams.actionArg4);
        
        if (res === ERR_NOT_IN_RANGE)
            creep.moveToTarget(target);
        
        return res;
    },
    
    /*
    harvestEnergy : function(creep, actionInfo, manager)
    {
        if (creep.carry.energy == creep.carryCapacity)
            return false;
        
        var source;
//        if (creep.fatigue == 0)
//            source = creep.pos.findClosestByPath(FIND_DROPPED_ENERGY);
        
        if (!source)
        {
            var sourceId = actionInfo.energySource;
            if (sourceId)
                source = Game.getObjectById(sourceId);

            if (!source || source.energy == 0)
            {
                source = creep.pos.findClosestByPath(FIND_SOURCES_ACTIVE, {
                        filter: (s) => s.pos.roomName === actionInfo.roomToHarvest
                    });
                actionInfo.harvestActionName = 'harvest';
            }

            if (!source)
            {
                if (actionInfo.roomToHarvest === actionInfo.roomToCollect
                 && Game.rooms[actionInfo.roomToCollect].energyCapacity != 0
                 && Game.rooms[actionInfo.roomToCollect].energyAvailable < Game.rooms[actionInfo.roomToCollect].energyCapacity)
                {
                    source = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE)
                                     && s.store[RESOURCE_ENERGY] !== 0
                                     && s.pos.roomName === actionInfo.roomToHarvest
                            });
                    actionInfo.harvestActionName = 'transferToCreep';
                }
            }

            if (!source)
            {
                actionInfo.harvestActionName = '';
                return true;
            }

            actionInfo.energySource = source.id;
        }
        else
            actionInfo.harvestActionName = 'pickup';
        
        var res = this.doSimpleAction(creep, source, actionInfo.harvestActionName, RESOURCE_ENERGY);
        
        if (res === ERR_NOT_IN_RANGE)
            creep.moveToTarget(source);
        else if (res === 0 && actionInfo.harvestActionName == 'pickup')
        {
            actionInfo.sourceId = '';
            actionInfo.harvestActionName = '';
        }
        
        return true;
    },
    
    storeEnergy : function(creep, actionInfo, manager)
    {
        if (creep.carry.energy == 0)
            return 0;

        var startPos = creep.pos;
        if (creep.pos.roomName != actionInfo.roomToCollect)
            startPos = room.spawns[0].pos;
        
        var target = startPos.findClosestByRange(FIND_MY_STRUCTURES, {
            filter: (s) => (s.structureType == STRUCTURE_SPAWN
                         || s.structureType == STRUCTURE_EXTENSION
                         || s.structureType == STRUCTURE_TOWER)
                         && s.energy < s.energyCapacity
                         && s.pos.roomName === actionInfo.roomToCollect
                });
                
        if (!target)
        {
            target = startPos.findClosestByRange(FIND_STRUCTURES, {
                filter: (s) => (s.structureType == STRUCTURE_CONTAINER || s.structureType == STRUCTURE_STORAGE)
                             && s.store[RESOURCE_ENERGY] < s.storeCapacity
                             && s.pos.roomName === actionInfo.roomToCollect
                    });
        }
                
                
        if (!target)
            return 2;
        
        if (creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE)
            creep.moveToTarget(target);

        return 1;
    },
    */
    
    getAvailableEnergy : function (energySource)
    {
        if (energySource['energy'] != undefined)
            return energySource.energy;
        
        if (energySource['store'] != undefined)
            return energySource.store[RESOURCE_ENERGY];
        
        return 0;
    },
    
    getEnergy : function(creep, actionInfo, roomName, manager)
    {
        if (creep.carryCapacity === 0)
            return this.results.NO_CAPACITY;
        
        if (creep.carry.energy == creep.carryCapacity)
        {
            this.clearActionDefaultTarget(actionInfo);
            return this.results.ACTION_DONE;
        }
        
        var getFromContainer = this.makeLoadEnergyAction(creep.pos, roomName);        
        var getFromPowerSource = this.makeHarvestEnergyAction(creep.pos, roomName);
        
        var res = this.executeOnTarget([getFromContainer, getFromPowerSource], creep, actionInfo, {
            actionArg1 : RESOURCE_ENERGY,
            isCompleted : (s, a) => creepDefaultActions.getAvailableEnergy(s) == 0
        });
        
        return res == OK ? this.results.ACTION_IN_PROGRESS : res;
    },
    
    processAction : function(creep, actionInfo, manager)
    {
        switch (actionInfo.actionId)
        {
            default:
                manager.processAction(creep, actionInfo);
                break;
        };    
    },

    defaultRun : function(creep, manager, timeToRenew)
    {
        if (timeToRenew != -1)
        {
            if (creep.tryRenewCreep(timeToRenew))
                return;

            actions.processQueueAndActions(creep.memory, (ai) => this.processAction(creep, ai, manager), 1);

            return;        
        }
    }
};

module.exports = creepDefaultActions;

